<?php
session_start();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"><!-- InstanceBegin template="/Templates/paper.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="EditRegion2" -->





<!-- InstanceEndEditable -->
<title>長榮牧羊人 資訊志工網站</title>
  <meta name="description" content="free website template" />
  <meta name="keywords" content="enter your keywords here" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/jquery.easing.min.js"></script>
  <script type="text/javascript" src="js/jquery.lavalamp.min.js"></script>
  <script type="text/javascript"></script>
  <script src="SpryAssets/SpryEffects.js" type="text/javascript"></script>
  <script type="text/javascript">
  function MM_callJS(jsStr) { //v2.0
  return eval(jsStr)
  }
  function MM_effectGrowShrink(targetElement, duration, from, to, toggle, referHeight, growFromCenter)
  {
	  Spry.Effect.DoGrow(targetElement, {duration: duration, from: from, to: to, toggle: toggle, referHeight: referHeight, growCenter: growFromCenter});
	  }
	  function MM_swapImgRestore() { //v3.0
	  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	  }
	  function MM_preloadImages() { //v3.0
	  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
	  var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
	  if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	  }
	  function MM_findObj(n, d) { //v4.01
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		  d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
		  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
		  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
		  if(!x && d.getElementById) x=d.getElementById(n); return x;
		  }
		  function MM_swapImage() { //v3.0
		  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
		  if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
		  }
  
  
  
    $(function() {
      $("#lava_menu").lavaLamp({
        fx: "backout",
        speed: 700
      });
    });
  </script>
    <script type="text/javascript" src="js/jquery.nivo.slider.pack.js"></script>
    <script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider();
    });
    </script>

</head>

<body>
  
  <div id="main">	
	<div id="menubar">
    
      <ul class="lavaLampWithImage">
        <li><a href="home.php">首頁</a></li>
        <?php
          		if($_SESSION["login_status"]=="login_ok")
           		{
           			 echo "<li><a href=\"register.php\">志工報名</a></li>";
           		}
           		else
           		{
            		echo "";
            	}
        	?>
        	<?php
          		if($_SESSION["login_status"]=="login_ok")
           	 	{
            		echo "<li><a href=\"pictures.php\">觀賞照片</a></li>";
            	}
            	else
            	{
            		echo "";
            	}
        	?>
        	<?php
          		if($_SESSION["login_status"]=="login_ok")
            	{
            		echo "<li><a href=\"upload_file.php\">檔案上傳</a></li>";
            	}
            	else
            	{
            		echo "";
            	}
        	?>
        <li><a href="location.php">交通位置</a></li>
        <li><a href="add.php">留言板</a></li>
        <li><a href="contact.php">聯繫我們</a></li>
        <?php
          	if($_SESSION["login_status"]=="login_ok")
            {
            echo "";
			}
            else
            {
           	   echo "<li><a href=\"login.php\">登入</a><li>"; 
            }
        ?>
        <?php
          	if($_SESSION["login_status"]=="login_ok")
            {
            echo "<li><a href=\"logout.php\">登出</a><li>";
            }
            else
            {
            echo "";
            }
        ?>
      </ul>
	</div><!--close menubar-->	    
	<div id="site_content">        	  
	  <div id="header"> 
        <div id="header_name"> 	  
          <h1>CJCU資訊志工 <span>長榮牧羊人</span></h1>
        </div><!--close header_name-->	
        <div id="header_slogan"> 		
	      <h2>Campus E-service Volunteer Team</h2>
		 </div><!--close header_slogan-->	
      </div><!--close header-->	
	  <div id="banner_image">
	    <div id="slider-wrapper">        
          <div id="slider" class="nivoSlider">
            <img src="images/slide3.jpg" alt="" />
            <img src="images/slide4.jpg" alt="" />
            <img src="images/slide5.jpg" alt="" />
            <img src="images/slide6.jpg" alt="" />
		  </div>
		</div><!--close slider-wrapper-->
	  </div><!--close banner_image-->	
      		  
      <div id="content">
	  <!-- InstanceBeginEditable name="EditRegion1" -->
      
        <div id="content_item"class="content_item">
        <div id="login">
          	 <?php
			if($_SESSION["login_status"]=="login_ok"){
				echo "<b>您好!</b> ".$_SESSION["login_name"]."&nbsp;";
		  $date=strtotime(date("H:i:s"));
		  $date_morning_start=strtotime("05:40:40");
		  $date_morning_end=strtotime("11:40:40");
		  $date_noon_start=strtotime("11:40:40");
		  $date_noon_end=strtotime("12:40:40");
		  $date_afternoon_start=strtotime("12:40:40");
		  $date_afternoon_end=strtotime("17:40:40");
		  $date_dinner_start=strtotime("17:40:40");
		  $date_dinner_end=strtotime("18:40:40");
		  $date_night_start=strtotime("18:40:40");
		  $date_night_end=strtotime("23:40:40");
          if($date_morning_start <=$date && $date < $date_morning_end)
		  {
			  echo "<b>早安=)</b><br>";
		  }
          elseif($date_noon_start <= $date && $date < $date_noon_end)
		  {
        	  echo "<b>午餐吃了沒?</b><br>";
		  }
		  elseif($date_afternoon_start <= $date && $date < $date_afternoon_end)
		  {
        	  echo "<b>下午好~</b><br>";
		  }
		  elseif($date_dinner_start <= $date && $date < $date_dinner_end)
		  {
        	  echo "<b>晚餐吃了沒?</b>?<br>";
		  }
		   elseif($date_night_start <= $date && $date < $date_night_end)
		  {
        	  echo "<b>晚安，別熬夜喔!!!</b><br>";
		  }
		  else
		  {
			  echo "<b>快洗洗睡，熬夜會早死!</b><br>";
		  }
		  }
          ?>
		  </div>
        <h1>燕巢DOC交通位置</h1>
        <center>
        <p><p>
          <div id="map">
            <div id="mymap"> 
            	<iframe width="500" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://my.ctrlq.org/maps/#roadmap|17|22.79337|120.36208690000001"></iframe>
                </div>
            <p></p>
          </div><p><p id="location">
          <?php
        require("require_location.php");
		srand((double)microtime()*1000000);
		$randval = rand(0,3);
		//echo $location[$randval];
		foreach($location as $new_loction){
			echo "<div  style=\"text-align:left\">".$new_loction."<br><br></div>";
			}
		?></p>
        
        </div><!--close content_item-->	
        <!-- InstanceEndEditable -->
		<div class="sidebar_container">   		  
		  <div class="sidebar">
            <div class="sidebar_item">
                <h2>志工點滴</h2>
			    <h4>Jun 2016</h4>
                <p><embed type="application/x-shockwave-flash" src="https://photos.gstatic.com/media/slideshow.swf" width="192" height="192" flashvars="host=picasaweb.google.com&hl=en_US&feat=flashalbum&RGB=0x000000&feed=https%3A%2F%2Fpicasaweb.google.com%2Fdata%2Ffeed%2Fapi%2Fuser%2F113748593613249236718%2Falbumid%2F6293353599341821633%3Falt%3Drss%26kind%3Dphoto%26authkey%3DGv1sRgCJmsxZO416a5zQE%26hl%3Den_US" pluginspage="http://www.macromedia.com/go/getflashplayer"></embed></p>
		          <!-- <a href="#">Read more</a> -->
              </div><!--close sidebar_item--> 
          </div><!--close sidebar--><!--close sidebar-->  
		  <div class="sidebar">
            <div class="sidebar_item">
              <h2>長榮大學</h2>
			  <!fb><iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fcjcu.tw&tabs=timeline&width=185&height=420&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="185" height="420" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe><!fb end>
		        <!-- <a href="#">Read more</a> -->
            </div><!--close sidebar_item--> 
          </div><!--close sidebar-->  
          <div class="sidebar">
           	<div class="sidebar_item">
              <h2>Facebook</h2>
              <h4>分享</h4>
              <p> 
              <img src="images/share.png" width="120" onclick="MM_callJS('javascript: void(window.open(\'http://www.facebook.com/share.php?u=\'.concat(encodeURIComponent(location.href)) ));');
              MM_effectGrowShrink(this, 1000, '50%', '100%', true, false, true)" hight="66"/> 
               <a href="javascript: void(window.open('http://www.facebook.com/share.php?u='.concat(encodeURIComponent(location.href)) ));"> </a>
              </p>
            </div><!--close sidebar_item--> 
          </div><!--close sidebar-->   
        </div><!--close sidebar_container-->	
       <br style="clear:both;" />
      </div><!--close content-->	
    </div><!--close site_content-->	
    <div id="footer">  
	  <div id="footer_content">
      <fb案讚><center><iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcjcu.tw&width=450&layout=standard&action=like&show_faces=true&share=true&height=80&appId" width="450" height="80" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe></center><fb案讚 end>
        <a href="http://validator.w3.org/check?uri=referer">Valid XHTML</a> | <a href="http://jigsaw.w3.org/css-validator/check/referer">Valid CSS</a> | <a href="http://fotogrph.com/">Images</a> | website template by <a href="http://www.araynordesign.co.uk">ARaynorDesign</a>
      </div><!--close footer_content-->	
    </div><!--close footer-->	
  </div><!--close main-->	
</body>
<!-- InstanceEnd --></html>
