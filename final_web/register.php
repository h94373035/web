<?php
session_start();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"><!-- InstanceBegin template="/Templates/paper.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="EditRegion2" -->





<!-- InstanceEndEditable -->
<title>長榮牧羊人 資訊志工網站</title>
  <meta name="description" content="free website template" />
  <meta name="keywords" content="enter your keywords here" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/jquery.easing.min.js"></script>
  <script type="text/javascript" src="js/jquery.lavalamp.min.js"></script>
  <script type="text/javascript"></script>
  <script src="SpryAssets/SpryEffects.js" type="text/javascript"></script>
  <script type="text/javascript">
  function MM_callJS(jsStr) { //v2.0
  return eval(jsStr)
  }
  function MM_effectGrowShrink(targetElement, duration, from, to, toggle, referHeight, growFromCenter)
  {
	  Spry.Effect.DoGrow(targetElement, {duration: duration, from: from, to: to, toggle: toggle, referHeight: referHeight, growCenter: growFromCenter});
	  }
	  function MM_swapImgRestore() { //v3.0
	  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	  }
	  function MM_preloadImages() { //v3.0
	  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
	  var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
	  if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	  }
	  function MM_findObj(n, d) { //v4.01
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		  d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
		  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
		  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
		  if(!x && d.getElementById) x=d.getElementById(n); return x;
		  }
		  function MM_swapImage() { //v3.0
		  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
		  if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
		  }
  
  
  
    $(function() {
      $("#lava_menu").lavaLamp({
        fx: "backout",
        speed: 700
      });
    });
  </script>
    <script type="text/javascript" src="js/jquery.nivo.slider.pack.js"></script>
    <script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider();
    });
    </script>

</head>

<body>
  
  <div id="main">	
	<div id="menubar">
    
      <ul class="lavaLampWithImage">
        <li><a href="home.php">首頁</a></li>
        <?php
          		if($_SESSION["login_status"]=="login_ok")
           		{
           			 echo "<li><a href=\"register.php\">志工報名</a></li>";
           		}
           		else
           		{
            		echo "";
            	}
        	?>
        	<?php
          		if($_SESSION["login_status"]=="login_ok")
           	 	{
            		echo "<li><a href=\"pictures.php\">觀賞照片</a></li>";
            	}
            	else
            	{
            		echo "";
            	}
        	?>
        	<?php
          		if($_SESSION["login_status"]=="login_ok")
            	{
            		echo "<li><a href=\"upload_file.php\">檔案上傳</a></li>";
            	}
            	else
            	{
            		echo "";
            	}
        	?>
        <li><a href="location.php">交通位置</a></li>
        <li><a href="add.php">留言板</a></li>
        <li><a href="contact.php">聯繫我們</a></li>
        <?php
          	if($_SESSION["login_status"]=="login_ok")
            {
            echo "";
			}
            else
            {
           	   echo "<li><a href=\"login.php\">登入</a><li>"; 
            }
        ?>
        <?php
          	if($_SESSION["login_status"]=="login_ok")
            {
            echo "<li><a href=\"logout.php\">登出</a><li>";
            }
            else
            {
            echo "";
            }
        ?>
      </ul>
	</div><!--close menubar-->	    
	<div id="site_content">        	  
	  <div id="header"> 
        <div id="header_name"> 	  
          <h1>CJCU資訊志工 <span>長榮牧羊人</span></h1>
        </div><!--close header_name-->	
        <div id="header_slogan"> 		
	      <h2>Campus E-service Volunteer Team</h2>
		 </div><!--close header_slogan-->	
      </div><!--close header-->	
	  <div id="banner_image">
	    <div id="slider-wrapper">        
          <div id="slider" class="nivoSlider">
            <img src="images/slide3.jpg" alt="" />
            <img src="images/slide4.jpg" alt="" />
            <img src="images/slide5.jpg" alt="" />
            <img src="images/slide6.jpg" alt="" />
		  </div>
		</div><!--close slider-wrapper-->
	  </div><!--close banner_image-->	
      		  
      <div id="content">
	  <!-- InstanceBeginEditable name="EditRegion1" -->
      
        <div class="content_item">
        <div id="login">
          	 <?php
			if($_SESSION["login_status"]=="login_ok"){
				echo "<b>您好!</b> ".$_SESSION["login_name"]."&nbsp;";
		  $date=strtotime(date("H:i:s"));
		  $date_morning_start=strtotime("05:40:40");
		  $date_morning_end=strtotime("11:40:40");
		  $date_noon_start=strtotime("11:40:40");
		  $date_noon_end=strtotime("12:40:40");
		  $date_afternoon_start=strtotime("12:40:40");
		  $date_afternoon_end=strtotime("17:40:40");
		  $date_dinner_start=strtotime("17:40:40");
		  $date_dinner_end=strtotime("18:40:40");
		  $date_night_start=strtotime("18:40:40");
		  $date_night_end=strtotime("23:40:40");
          if($date_morning_start <=$date && $date < $date_morning_end)
		  {
			  echo "<b>早安=)</b><br>";
		  }
          elseif($date_noon_start <= $date && $date < $date_noon_end)
		  {
        	  echo "<b>午餐吃了沒?</b><br>";
		  }
		  elseif($date_afternoon_start <= $date && $date < $date_afternoon_end)
		  {
        	  echo "<b>下午好~</b><br>";
		  }
		  elseif($date_dinner_start <= $date && $date < $date_dinner_end)
		  {
        	  echo "<b>晚餐吃了沒?</b>?<br>";
		  }
		   elseif($date_night_start <= $date && $date < $date_night_end)
		  {
        	  echo "<b>晚安，別熬夜喔!!!</b><br>";
		  }
		  else
		  {
			  echo "<b>快洗洗睡，熬夜會早死!</b><br>";
		  }
		  }
          ?>
		  </div>
         <h1>資訊志工線上報名</h1><br><br>
        <center>
            <a href="http://webst.cjcu.edu.tw/~f74372017/final_web/register.php?color=gold">黃色報名表</a>
            <a href="http://webst.cjcu.edu.tw/~f74372017/final_web/register.php?color=skyblue">&nbsp;藍色報名表</a>
            <a href="http://webst.cjcu.edu.tw/~f74372017/final_web/register.php?color=purple">&nbsp;紫色報名表</a>
            <a href="http://webst.cjcu.edu.tw/~f74372017/final_web/register.php?color=pink">&nbsp;粉色報名表</a>
            <a href="http://webst.cjcu.edu.tw/~f74372017/final_web/register.php?color=orange">&nbsp;橘色報名表</a>
            <a href="http://webst.cjcu.edu.tw/~f74372017/final_web/register.php?color=red">&nbsp;紅色報名表</a>
            <br /><br />
            <img src="images/slogon.jpg"/>
            <br />
            <form action="register_receive.php" method="get">
             <?php
			 	$color=$_GET["color"];
				if($color=="gold"){echo "<table bgcolor=\"gold\"cellspacing=10>";}
				elseif($color=="skyblue"){echo "<table bgcolor=\"skyblue\" cellspacing=10>";}
				elseif($color=="purple"){echo "<table bgcolor=\"#FF00FF\" cellspacing=10>";}
				elseif($color=="pink"){echo "<table bgcolor=\"pink\" cellspacing=10>";}
				elseif($color=="orange"){echo "<table bgcolor=\"#FF8000\" cellspacing=10>";}
				elseif($color=="red"){echo "<table bgcolor=\"red\" cellspacing=10>";}
				else{echo "<table cellspacing=10>";}
			 ?>
                <tr ><td>學校名稱:</td>
                <td>
                <select name="school" size="1">
                <option value="長榮大學 ">長榮大學</option>
                <option value="國立成功大學">國立成功大學</option>
                <option value="國立臺南大學">國立臺南大學</option>
                <option value="崑山科技大學">崑山科技大學</option>
                <option value="南臺科技大學">南臺科技大學</option>
                <option value="嘉南藥理大學">嘉南藥理大學</option>
                <option value="遠東科技大學">遠東科技大學</option>
                <option value="中華醫事科技大學">中華醫事科技大學</option>
                <option value="台南應用科技大學">台南應用科技大學</option>
                </select>
                </td></tr>
                <tr><td>科系名稱:</td><td><input name="class" type="text" size="12" />*(請寫全名)</td></tr>
                <tr><td>職稱:</td><td><input name="job" type="text" size="4" /></td></tr>
                <tr><td>中文姓名:</td><td><input name="chinses_name" type="text" size="6" /></td></tr>
                <tr><td>英文姓名:</td><td><input name="english_name" type="text" size="8" /></td></tr>
                <tr><td>身分證字號:</td><td><input name="ID" type="text" size="10" /></td></tr>
                <tr><td>出生年月日:</td>
                <td><input name="birthday_year" type="text" size="2" />年(西元)<input name="birthday_month" type="text" size="1" />月<input name="birthday_day" type="text" size="1" />日</td></tr>
                <tr><td>手機:</td><td><input name="cellphone" type="text" size="10" /></td></tr>
                <tr><td>E-mail:</td><td><input name="email" type="text" size="20" /></td></tr>
                <tr><td>戶籍地址:</td><td><input name="address" type="text" size="40" /></td></tr>
                <tr><td>緊急聯絡人:</td><td><input name="emergency_contact" type="text" size="6" /></td></tr>
                <tr><td>緊急聯絡人電話:</td><td><input name="emergency_contact_cellphone" type="text" size="10" /></td></tr>
                <tr><td>監護人:</td><td><input name="guardian" type="text" size="6" /></td></tr>
       			<tr><td>監護人電話:</td><td><input name="guardian_cellphone" type="text" size="10" /></td></tr>
        		<tr><td>身分別:</td>
                <td>
                <input name="identity" type="radio" value="一班生" checked />一班生
                <input name="identity" type="radio" value="原住民" />原住民
                <input name="identity" type="radio" value="外籍生" />外籍生
                <input name="identity" type="radio" value="僑生" />僑生
                <input name="identity" type="radio" value="其他" />其他
                </td></tr>
        		</table>
                <input type="submit" value="  送出  " /><input type="reset" value="  清除  "></form>
                
        		
        
        </div><!--close content_item-->	
	      <!-- InstanceEndEditable -->
		<div class="sidebar_container">   		  
		  <div class="sidebar">
            <div class="sidebar_item">
                <h2>志工點滴</h2>
			    <h4>Jun 2016</h4>
                <p><embed type="application/x-shockwave-flash" src="https://photos.gstatic.com/media/slideshow.swf" width="192" height="192" flashvars="host=picasaweb.google.com&hl=en_US&feat=flashalbum&RGB=0x000000&feed=https%3A%2F%2Fpicasaweb.google.com%2Fdata%2Ffeed%2Fapi%2Fuser%2F113748593613249236718%2Falbumid%2F6293353599341821633%3Falt%3Drss%26kind%3Dphoto%26authkey%3DGv1sRgCJmsxZO416a5zQE%26hl%3Den_US" pluginspage="http://www.macromedia.com/go/getflashplayer"></embed></p>
		          <!-- <a href="#">Read more</a> -->
              </div><!--close sidebar_item--> 
          </div><!--close sidebar--><!--close sidebar-->  
		  <div class="sidebar">
            <div class="sidebar_item">
              <h2>長榮大學</h2>
			  <!fb><iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fcjcu.tw&tabs=timeline&width=185&height=420&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="185" height="420" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe><!fb end>
		        <!-- <a href="#">Read more</a> -->
            </div><!--close sidebar_item--> 
          </div><!--close sidebar-->  
          <div class="sidebar">
           	<div class="sidebar_item">
              <h2>Facebook</h2>
              <h4>分享</h4>
              <p> 
              <img src="images/share.png" width="120" onclick="MM_callJS('javascript: void(window.open(\'http://www.facebook.com/share.php?u=\'.concat(encodeURIComponent(location.href)) ));');
              MM_effectGrowShrink(this, 1000, '50%', '100%', true, false, true)" hight="66"/> 
               <a href="javascript: void(window.open('http://www.facebook.com/share.php?u='.concat(encodeURIComponent(location.href)) ));"> </a>
              </p>
            </div><!--close sidebar_item--> 
          </div><!--close sidebar-->   
        </div><!--close sidebar_container-->	
       <br style="clear:both;" />
      </div><!--close content-->	
    </div><!--close site_content-->	
    <div id="footer">  
	  <div id="footer_content">
      <fb案讚><center><iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcjcu.tw&width=450&layout=standard&action=like&show_faces=true&share=true&height=80&appId" width="450" height="80" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe></center><fb案讚 end>
        <a href="http://validator.w3.org/check?uri=referer">Valid XHTML</a> | <a href="http://jigsaw.w3.org/css-validator/check/referer">Valid CSS</a> | <a href="http://fotogrph.com/">Images</a> | website template by <a href="http://www.araynordesign.co.uk">ARaynorDesign</a>
      </div><!--close footer_content-->	
    </div><!--close footer-->	
  </div><!--close main-->	
</body>
<!-- InstanceEnd --></html>
