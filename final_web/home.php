<?php
session_start();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"><!-- InstanceBegin template="/Templates/paper.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="EditRegion2" -->





<!-- InstanceEndEditable -->
<title>長榮牧羊人 資訊志工網站</title>
  <meta name="description" content="free website template" />
  <meta name="keywords" content="enter your keywords here" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/jquery.easing.min.js"></script>
  <script type="text/javascript" src="js/jquery.lavalamp.min.js"></script>
  <script type="text/javascript"></script>
  <script src="SpryAssets/SpryEffects.js" type="text/javascript"></script>
  <script type="text/javascript">
  function MM_callJS(jsStr) { //v2.0
  return eval(jsStr)
  }
  function MM_effectGrowShrink(targetElement, duration, from, to, toggle, referHeight, growFromCenter)
  {
	  Spry.Effect.DoGrow(targetElement, {duration: duration, from: from, to: to, toggle: toggle, referHeight: referHeight, growCenter: growFromCenter});
	  }
	  function MM_swapImgRestore() { //v3.0
	  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	  }
	  function MM_preloadImages() { //v3.0
	  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
	  var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
	  if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	  }
	  function MM_findObj(n, d) { //v4.01
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
		  d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
		  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
		  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
		  if(!x && d.getElementById) x=d.getElementById(n); return x;
		  }
		  function MM_swapImage() { //v3.0
		  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
		  if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
		  }
  
  
  
    $(function() {
      $("#lava_menu").lavaLamp({
        fx: "backout",
        speed: 700
      });
    });
  </script>
    <script type="text/javascript" src="js/jquery.nivo.slider.pack.js"></script>
    <script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider();
    });
    </script>

</head>

<body onload="MM_preloadImages('images/water2.jpg','images/swall2.JPG')">
  
  <div id="main">	
	<div id="menubar">
    
      <ul class="lavaLampWithImage">
        <li><a href="home.php">首頁</a></li>
        <?php
          		if($_SESSION["login_status"]=="login_ok")
           		{
           			 echo "<li><a href=\"register.php\">志工報名</a></li>";
           		}
           		else
           		{
            		echo "";
            	}
        	?>
        	<?php
          		if($_SESSION["login_status"]=="login_ok")
           	 	{
            		echo "<li><a href=\"pictures.php\">觀賞照片</a></li>";
            	}
            	else
            	{
            		echo "";
            	}
        	?>
        	<?php
          		if($_SESSION["login_status"]=="login_ok")
            	{
            		echo "<li><a href=\"upload_file.php\">檔案上傳</a></li>";
            	}
            	else
            	{
            		echo "";
            	}
        	?>
        <li><a href="location.php">交通位置</a></li>
        <li><a href="add.php">留言板</a></li>
        <li><a href="contact.php">聯繫我們</a></li>
        <?php
          	if($_SESSION["login_status"]=="login_ok")
            {
            echo "";
			}
            else
            {
           	   echo "<li><a href=\"login.php\">登入</a><li>"; 
            }
        ?>
        <?php
          	if($_SESSION["login_status"]=="login_ok")
            {
            echo "<li><a href=\"logout.php\">登出</a><li>";
            }
            else
            {
            echo "";
            }
        ?>
      </ul>
	</div><!--close menubar-->	    
	<div id="site_content">        	  
	  <div id="header"> 
        <div id="header_name"> 	  
          <h1>CJCU資訊志工 <span>長榮牧羊人</span></h1>
        </div><!--close header_name-->	
        <div id="header_slogan"> 		
	      <h2>Campus E-service Volunteer Team</h2>
		 </div><!--close header_slogan-->	
      </div><!--close header-->	
	  <div id="banner_image">
	    <div id="slider-wrapper">        
          <div id="slider" class="nivoSlider">
            <img src="images/slide3.jpg" alt="" />
            <img src="images/slide4.jpg" alt="" />
            <img src="images/slide5.jpg" alt="" />
            <img src="images/slide6.jpg" alt="" />
		  </div>
		</div><!--close slider-wrapper-->
	  </div><!--close banner_image-->	
      		  
      <div id="content">
	  <!-- InstanceBeginEditable name="EditRegion1" -->
      
        <div class="content_item">
         <div id="login">
          	 <?php
			if($_SESSION["login_status"]=="login_ok"){
				echo "<b>您好!</b> ".$_SESSION["login_name"]."&nbsp;";
		  $date=strtotime(date("H:i:s"));
		  $date_morning_start=strtotime("05:40:40");
		  $date_morning_end=strtotime("11:40:40");
		  $date_noon_start=strtotime("11:40:40");
		  $date_noon_end=strtotime("12:40:40");
		  $date_afternoon_start=strtotime("12:40:40");
		  $date_afternoon_end=strtotime("17:40:40");
		  $date_dinner_start=strtotime("17:40:40");
		  $date_dinner_end=strtotime("18:40:40");
		  $date_night_start=strtotime("18:40:40");
		  $date_night_end=strtotime("23:40:40");
          if($date_morning_start <=$date && $date < $date_morning_end)
		  {
			  echo "<b>早安=)</b><br>";
		  }
          elseif($date_noon_start <= $date && $date < $date_noon_end)
		  {
        	  echo "<b>午餐吃了沒?</b><br>";
		  }
		  elseif($date_afternoon_start <= $date && $date < $date_afternoon_end)
		  {
        	  echo "<b>下午好~</b><br>";
		  }
		  elseif($date_dinner_start <= $date && $date < $date_dinner_end)
		  {
        	  echo "<b>晚餐吃了沒?</b>?<br>";
		  }
		   elseif($date_night_start <= $date && $date < $date_night_end)
		  {
        	  echo "<b>晚安，別熬夜喔!!!</b><br>";
		  }
		  else
		  {
			  echo "<b>快洗洗睡，熬夜會早死!</b><br>";
		  }
		  }
          ?>
		  </div>
          <h1>阿公沒水喝暑期營隊</h1>
		  <br>
          <a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image6','','images/water2.jpg',1)"><img src="images/water.jpg" width="468" height="300" id="Image6" /></a><br> 
          預期效益：<br>

			1.活動結束後小朋友能更了解水庫的運作方式以及水資源的重要性<br>

			2.小朋友能了解團隊合作的重要性以及默契的培養<br>

			3.對電腦繪圖有更進一步的認識並且會使用電繪板畫圖<br>
          <h1>早起的燕子有棗吃寒假營隊</h1>
          <a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image7','','images/swall2.JPG',1)"><img src="images/swall1.JPG" width="468" height="300" id="Image7" /></a><br>
          預期效益：<br>

			1.參觀完蜜餞工廠能讓小朋友對自己的家鄉特產有進一步的了解<br>

			2.小朋友能了解團隊合作的重要性以及默契的培養<br>

			3.能靈活運用電繪板構圖並且能運用拼貼方式製作EDM<br><br><br>
          
--------------------------瀏覽人次---------------------------<br>
          <?php 
				$file=@file("count.txt"); 
				list($today,$date,$num,$yesterday)=explode(",",$file[0]); 
				$d=getdate(); 
				$ty=$d["year"]; 
				$tm=$d["mon"]; 
				$td=$d["mday"]; 
				$tdate=date("y-m-d");//取得今天日期 
				$ydate=date("y-m-d",mktime(0,0,0,$tm,$td-1,$ty));//取得今天日期 
				if($date=="$tdate")
				{ 
					if(strlen($yesterday)==0)
					{ 
						$yesterday="0"; 
					} 
					/////////////////////////////////////// 
					$ip = $_SERVER['REMOTE_ADDR']; 
					$lastip = @file_get_contents("ip.txt"); 
					if ( $ip != $lastip ) 
					{ 
						file_put_contents("ip.txt",$ip); 
						$today++; //今日+1 
						$num++; //總數+1 
					} 
					/////////////////////////////////////// 
					// $today++; //今日+1 將這兩句移入 IP 測試區塊 
					// $num++; //總數+1 
					$w=fopen("count.txt","w"); 
					fputs($w,$today.",".$tdate.",".$num.",".$yesterday); 
					fclose($w); 
				}
				else
				{ 
					$num++; //總數+1 
					if($date=="$ydate")
					{ 
						$yesterday=$today; 
					}
					else
					{ 
						$yesterday=0; 
					} 
					$today=1; //如果不是今天的日期,那麼今日人數歸為1 
					$w=fopen("count.txt","w"); 
					fputs($w,$today.",".$tdate.",".$num.",".$yesterday); //把新日期也存入 
					fclose($w); 
				} 
				if($yesterday=="")
				{ 
					$yesterday="0"; 
				} 
				echo "今日瀏覽人次: ".$today."<br>"; 
				echo "昨日瀏覽人次: ".$yesterday."<br>"; 
				echo "累積瀏覽人次: ".$num; 
			?> 
              <?php  
			  	/*$file=@file("count.txt");
				
				list($today,$date,$num)=explode(",",$file[0]); //用 list() 函式將3個陣列元素載入變數
				$tdate=date("y-m-d");//取得今天日期
				
				if($_SESSION['count'] != "yes"){
					if($date=="$tdate")
					{
						$today++; //以從檔案讀出之今日總數+1
						$num++; //以從檔案讀出之全部總數+1
								
						$w=fopen("count.txt","w");
						fputs($w,$today.",".$tdate.",".$num); //將新資料寫進檔案
						fclose($w);
						$_SESSION['count']="yes";
					}	
					else
					{
						$today=1; //如果不是今天的日期,那麼今日人數歸為1 
						$num++; //總數+0
					
						$w=fopen("count.txt","w");
						fputs($w,$today.",".$tdate.",".$num); //將新資料寫進檔案\
						fclose($w);
						$_SESSION['count']="yes";
					}
				}
				
				
				echo "今日瀏覽人次: ".$today."<br>";
				echo "累積瀏覽人次: ".$num."<br>";*/
				?>
          </p>
        </div><!--close content_item-->	
	      <!-- InstanceEndEditable -->
		<div class="sidebar_container">   		  
		  <div class="sidebar">
            <div class="sidebar_item">
                <h2>志工點滴</h2>
			    <h4>Jun 2016</h4>
                <p><embed type="application/x-shockwave-flash" src="https://photos.gstatic.com/media/slideshow.swf" width="192" height="192" flashvars="host=picasaweb.google.com&hl=en_US&feat=flashalbum&RGB=0x000000&feed=https%3A%2F%2Fpicasaweb.google.com%2Fdata%2Ffeed%2Fapi%2Fuser%2F113748593613249236718%2Falbumid%2F6293353599341821633%3Falt%3Drss%26kind%3Dphoto%26authkey%3DGv1sRgCJmsxZO416a5zQE%26hl%3Den_US" pluginspage="http://www.macromedia.com/go/getflashplayer"></embed></p>
		          <!-- <a href="#">Read more</a> -->
              </div><!--close sidebar_item--> 
          </div><!--close sidebar--><!--close sidebar-->  
		  <div class="sidebar">
            <div class="sidebar_item">
              <h2>長榮大學</h2>
			  <!fb><iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fcjcu.tw&tabs=timeline&width=185&height=420&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="185" height="420" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe><!fb end>
		        <!-- <a href="#">Read more</a> -->
            </div><!--close sidebar_item--> 
          </div><!--close sidebar-->  
          <div class="sidebar">
           	<div class="sidebar_item">
              <h2>Facebook</h2>
              <h4>分享</h4>
              <p> 
              <img src="images/share.png" width="120" onclick="MM_callJS('javascript: void(window.open(\'http://www.facebook.com/share.php?u=\'.concat(encodeURIComponent(location.href)) ));');
              MM_effectGrowShrink(this, 1000, '50%', '100%', true, false, true)" hight="66"/> 
               <a href="javascript: void(window.open('http://www.facebook.com/share.php?u='.concat(encodeURIComponent(location.href)) ));"> </a>
              </p>
            </div><!--close sidebar_item--> 
          </div><!--close sidebar-->   
        </div><!--close sidebar_container-->	
       <br style="clear:both;" />
      </div><!--close content-->	
    </div><!--close site_content-->	
    <div id="footer">  
	  <div id="footer_content">
      <fb案讚><center><iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcjcu.tw&width=450&layout=standard&action=like&show_faces=true&share=true&height=80&appId" width="450" height="80" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe></center><fb案讚 end>
        <a href="http://validator.w3.org/check?uri=referer">Valid XHTML</a> | <a href="http://jigsaw.w3.org/css-validator/check/referer">Valid CSS</a> | <a href="http://fotogrph.com/">Images</a> | website template by <a href="http://www.araynordesign.co.uk">ARaynorDesign</a>
      </div><!--close footer_content-->	
    </div><!--close footer-->	
  </div><!--close main-->	
</body>
<!-- InstanceEnd --></html>
